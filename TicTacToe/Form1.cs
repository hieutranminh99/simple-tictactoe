﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public int player = 2;
        public int turns = 0; //counting turns;


        private void btnClick(object sender, EventArgs e)
        {
            Button button = (Button) sender;
            if (button.Text == "")
            {
                if (player % 2 == 0)
                {
                    button.Text = "X";
                    player++;
                    turns++;
                }
                else
                {
                    button.Text = "O";
                    player++;
                    turns++;
                }

                if (checkWinner() == true)
                {
                    if (button.Text == "X")
                    {
                        MessageBox.Show("X Won!");
                    }
                    else if (button.Text == "O")
                    {
                        MessageBox.Show("O Won!");
                    }
                }

                else if (checkDraw() == true)
                {
                    MessageBox.Show("Tie Game!");
                    NewGame();
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            NewGame();
        }

        void NewGame()
        {
            player = 2;
            turns = 0;
            button1.Text = button2.Text = button3.Text =
                button4.Text = button5.Text = button6.Text = button7.Text = button8.Text = button9.Text = "";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            NewGame();
        }

        bool checkDraw()
        {
            if (turns == 9)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool checkWinner()
        {
            if ((button1.Text == button4.Text) && (button4.Text == button7.Text) && button1.Text != "")
            {
                return true;
            }
            if ((button2.Text == button5.Text) && (button5.Text == button8.Text) && button2.Text != "")
            {
                return true;
            }
            if ((button3.Text == button6.Text) && (button6.Text == button9.Text) && button3.Text != "")
            {
                return true;
            }
            if ((button1.Text == button2.Text) && (button2.Text == button3.Text) && button1.Text != "")
            {
                return true;
            }
            if ((button4.Text == button5.Text) && (button5.Text == button6.Text) && button4.Text != "")
            {
                return true;
            }
            if ((button7.Text == button8.Text) && (button8.Text == button9.Text) && button7.Text != "")
            {
                return true;
            }
            if ((button1.Text == button5.Text) && (button5.Text == button9.Text) && button1.Text != "")
            {
                return true;
            }
            if ((button3.Text == button5.Text) && (button5.Text == button7.Text) && button3.Text != "")
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
